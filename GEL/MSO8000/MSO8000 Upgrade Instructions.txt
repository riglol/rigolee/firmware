This documentation is designed to provide guidance to customers on how to 
upgrade the MSO8000 series digital oscilloscope to the version in the 
"MSO8000(ARM)update.rar" file. 

 
New versions of firmware are available at the following website:
http://int.rigol.com/Support/SoftDownload/3

[Notices]

    - Before upgrading, please carefully refer to the 
      "MSO8000 Release Notes.txt" file to obtain the updated information of
      the firmware version. 
      
    - Please make sure your USB disk can be read correctly. MSO8000 series 
      is required to use a USB Flash drive disk with FAT32 format. 
      
    - During the upgrade process, please do not cut off power and pull out the 
      USB disk, otherwise the instrument will fail to work normally.
       

[Upgrading Procedures]

    1. Copy the upgrade file DS8000Update.GEL to the root directory of the USB
       disk. 
       
    2. Keep the instrument in the power-on state, then insert the USB disk into
       the USB HOST interface (next to the Power key). 
       
    3. After the U disk is successfully identified, you need to press
       Utility -> System -> Help -> Local upgrade -> OK to start. 
       
    4. During the upgrade, the instrument interface will display the current 
       upgrade progress. 
       
    5. After upgrading has been completed, the instrument will restart. 
    
    6. Check whether the firmware version number has been updated 
       (press Utility -> System -> About). 
       
    7. After upgrading has been completed, perform the self-calibration 
       operation. Make sure that the instrument has been warmed up or has been
       operating for more than 30 minutes before the self-calibration. 
       
        a. Disconnect all input channels; 
        
        b. Press Utility->System-> SelfCal, then click Start to perform the 
           self-calibration. 
           
        c. It takes about 105 minutes to perform the self-calibration. 
        
        d. After the calibration has been completed, restart the instrument.

[Troubleshooting]

    - If the instrument failed to read the upgrade file from the USB disk, 
      please try to replace the USB disk. 
      
    - If the problem still persists, please try to re-download the upgrade file.
    
    - If the problem still persists after performing the above two operations, 
      please visit the website int.rigol.com to find contacts in your area. 