/*
 * Copyright (C) 2019 Olliver Schinagl <oliver@schinagl.nl>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

/dts-v1/;

/ {
	description = "kerstrel Kernel Image generated with Linux kernel and FDT blob";
	#address-cells = <1>;

	images {
		kernel@1 {
			description = "Kerstrel Linux kernel";
			data = /incbin/("./zImage");
			type = "kernel";
			arch = "arm";
			os = "linux";
			compression = "none";
			load = <0x00100000>;
			entry = <0x00100000>;
			hash-1 {
				algo = "sha1";
			};
		};

		fdt@1 {
			description = "Flattened Device Tree blob";
			data = /incbin/("./kerstrel.dtb");
			type = "flat_dt";
			arch = "arm";
			os = "linux";
			compression = "none";
			hash-1 {
				algo = "sha1";
			};
		};

		ramdisk@1 {
			description = "kerstrel-Update-Ramdisk";
			data = /incbin/("./initrd.ext2.img.gz");
			type = "ramdisk";
			arch = "arm";
			os = "linux";
			compression = "gzip";
			hash-1 {
				algo = "sha1";
			};
		};
	};

	configurations {
		default = "rootfs@1";

		rootfs@1 {
			description = "Boot Linux kernel with FDT blob";
			kernel = "kernel@1";
			ramdisk = "ramdisk@1";
			fdt = "fdt@1";
		};
	};
};
