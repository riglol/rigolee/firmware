
var buttonComPre = ":SYSTem:KEY:PRESs" + ' ';
var knobIncComPre = ":SYSTem:KEY:INCRease" + ' ';
var knobDecComPre = ":SYSTem:KEY:DECRease" + ' ';
var buttonCommandArray = [
    //面板右侧9个竖排按钮编号[0~8]

    buttonComPre + 'MOFF\n' ,
    buttonComPre + 'F1\n' ,
    buttonComPre + 'F2\n' ,
    buttonComPre + 'F3\n' ,
    buttonComPre + 'F4\n' ,
    buttonComPre + 'F5\n' ,
    buttonComPre + 'F6\n' ,
    buttonComPre + 'F7\n' ,
    buttonComPre + 'CLEar\n' ,

    //面板上部第一排按钮
    //多功能旋钮
    knobIncComPre + 'KFUNction\n' ,
    buttonComPre + 'KFUNction\n' ,
    knobDecComPre + 'KFUNction\n' ,

    //缺少Default
    buttonComPre + 'CLEar\n' ,
    buttonComPre + 'AUTO\n' ,
    buttonComPre + 'RSTop\n' ,
    buttonComPre + 'SINGle\n' ,
    //面板中部Menu控制区按钮
    //缺少Quick
    buttonComPre + 'MEASure\n' ,
    buttonComPre + 'ACQuire\n' ,
    buttonComPre + 'STORage\n' ,
    buttonComPre + 'CURSor\n' ,
    buttonComPre + 'DISPlay\n' ,
    buttonComPre + 'UTILity\n' ,


    //面板中部Horizontal控制区按钮

    //波形录制按钮
    //停止录制
    buttonComPre + 'ERECord\n' ,
    //开始、暂停或继续回放已录制的波形
    buttonComPre + 'PPAuse\n' ,
    //开始录制
    buttonComPre + 'SRECord\n' ,

    //缺少Navigate
    //缺少ZOOM

    //水平时基旋钮
    knobIncComPre + 'HSCale\n' ,
    buttonComPre + 'HSCale\n' ,
    knobDecComPre + 'HSCale\n' ,
    //水平位移旋钮
    knobIncComPre + 'HPOSition\n' ,
    buttonComPre + 'HPOSition\n' ,
    knobDecComPre + 'HPOSition\n' ,

    //面板中部控制区按钮Wave控制区按钮
    //缺少position旋钮
    //缺少scale旋钮
    //缺少Digial 按键
    buttonComPre + 'MATH\n' ,
    buttonComPre + 'REF\n' ,
    buttonComPre + 'DECode1\n' ,


    //面板中部控制区按钮Trigger控制区按钮
    buttonComPre + 'HELP\n' ,
    buttonComPre + 'TMENu\n' ,
    buttonComPre + 'TMODe\n' ,
    //触发电平旋钮
    knobIncComPre + 'TLEVel\n' ,
    buttonComPre + 'TLEVel\n' ,
    knobDecComPre + 'TLEVel\n' ,

    //面板下部Vertical控制区按钮

    //通道1
    //通道1垂直位移旋钮
    knobIncComPre + 'VPOSition1\n' ,
    buttonComPre + 'VPOSition1\n' ,
    knobDecComPre + 'VPOSition1\n' ,
    //CH1 通道按钮
    buttonComPre + 'CH1\n' ,
    //通道1垂直档位旋钮
    knobIncComPre + 'VSCale1\n' ,
    buttonComPre + 'VSCale1\n' ,
    knobDecComPre + 'VSCale1\n' ,

    //通道2
    //通道2垂直位移旋钮
    knobIncComPre + 'VPOSition2\n' ,
    buttonComPre + 'VPOSition2\n' ,
    knobDecComPre + 'VPOSition2\n' ,
    //CH2 通道按钮
    buttonComPre + 'CH2\n' ,
    //通道2垂直档位旋钮
    knobIncComPre + 'VSCale2\n' ,
    buttonComPre + 'VSCale2\n' ,
    knobDecComPre + 'VSCale2\n' ,

    //通道3
    //通道3垂直位移旋钮
    knobIncComPre + 'VPOSition3\n' ,
    buttonComPre + 'VPOSition3\n' ,
    knobDecComPre + 'VPOSition3\n' ,
    //CH3 通道按钮
    buttonComPre + 'CH3\n' ,
    //通道3垂直档位旋钮
    knobIncComPre + 'VSCale3\n' ,
    buttonComPre + 'VSCale3\n' ,
    knobDecComPre + 'VSCale3\n' ,


    //通道4
    //通道4垂直位移旋钮
    knobIncComPre + 'VPOSition4\n' ,
    buttonComPre + 'VPOSition4\n' ,
    knobDecComPre + 'VPOSition4\n' ,
    //CH4 通道按钮
    buttonComPre + 'CH4\n' ,
    //通道4垂直档位旋钮
    knobIncComPre + 'VSCale4\n' ,
    buttonComPre + 'VSCale4\n' ,
    knobDecComPre + 'VSCale4\n' ,


    //source 1
    buttonComPre + 'SOURce1\n' ,

    //source 2
    buttonComPre + 'SOURce2\n' ,
    //缺少手型按钮




]